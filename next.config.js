const { PHASE_DEVELOPMENT_SERVER, PHASE_PRODUCTION_BUILD } = require('next/constants')

module.exports = phase => {
  const isDev = phase === PHASE_DEVELOPMENT_SERVER

  const isProd = phase === PHASE_PRODUCTION_BUILD && process.env.STAGING !== '1'

  const isStaging = phase === PHASE_PRODUCTION_BUILD && process.env.STAGING === '1'

  const devOrPreviewEnv = {
    BASE_URL: 'http://localhost:55555'
  }

  const prodEnv = {
    BASE_URL: 'https://api.fariz.delman.io'
  }

  const env = isProd ? prodEnv : devOrPreviewEnv

  return {
    env
  }
}

project = "unicorn_project"

app "unicorn_app" {
  build {
    use "docker" {}
  }

  deploy { 
    use "docker" {}
  }
}

import Link from 'next/link'

const IndexPage = () => (
  <div>
    <h2>Hello World</h2>
    <p>
      <Link href='/about'>
        <a>About me</a>
      </Link>
    </p>
  </div>
)

export default IndexPage

import Link from 'next/link'

const AboutPage = ({ data = [] }) => (
  <div>
    <p>Hi!</p>
    <ul>
      {data.map(a => (
        <li key={a.id} data-testid='awesome'>
          {a.id}
        </li>
      ))}
    </ul>
    <p>
      <Link href='/'>
        <a>Back to home</a>
      </Link>
    </p>
  </div>
)

export async function getServerSideProps(ctx) {
  const BASE_URL = process.env.BASE_URL

  const res = await fetch(`${BASE_URL}/api.json`)
  const { data } = await res.json()

  return {
    props: {
      data
    }
  }
}

export default AboutPage

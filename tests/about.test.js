import { render, screen, waitFor } from '@testing-library/react'

import AboutPage from '../pages/about'

global.fetch = jest.fn(() => Promise.resolve({ data: [{ id: 0 }, { id: 1 }, { id: 2 }] }))

describe('About page', () => {
  it('Should render About page', () => {
    render(<AboutPage />)

    const hello = screen.getByText(/hi/i)

    expect(hello).toBeInTheDocument()
  })

  it('Should render list of data', async () => {
    const data = [{ id: 0 }, { id: 1 }, { id: 2 }]

    const { getAllByTestId } = render(<AboutPage data={data} />)

    expect(getAllByTestId('awesome')).toHaveLength(3)
  })
})

import { render, screen } from '@testing-library/react'

import IndexPage from '../pages/index'

describe('Index page', () => {
  it('Should render Index page dong', () => {
    render(<IndexPage />)

    const hello = screen.getByText(/hello world/i)

    expect(hello).toBeInTheDocument()
  })
})

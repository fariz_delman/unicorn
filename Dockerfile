FROM node:lts

ADD ./package.json /app/package.json
ADD ./yarn.lock /app/yarn.lock

WORKDIR /app

RUN yarn --production

ADD . /app

RUN yarn next build

EXPOSE 3000

CMD ["yarn", "start"]
